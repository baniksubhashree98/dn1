package com.thoughtworks.DailyNeeds.service;

import com.thoughtworks.DailyNeeds.entity.Version;
import com.thoughtworks.DailyNeeds.repository.VersionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class VersionServiceTest {

    @Autowired
    private VersionService versionService;

    @MockBean
    private VersionRepository versionRepository;

    @Test
    void shouldGetTheSameCurrentVersionWhenCurrentVersionIsGiven() {
        Version expectedCurrentVersion = Version.builder()
                .versionName("v1")
                .build();
        when(versionRepository.getCurrentVersion()).thenReturn(expectedCurrentVersion);

        Version actualCurrentVersion = versionService.getCurrentVersion();

        assertEquals(expectedCurrentVersion, actualCurrentVersion);
    }

    @Test
    void shouldGetNullAsCurrentVersionWhenCurrentVersionIsNotGiven() {
        when(versionRepository.getCurrentVersion()).thenReturn(null);

        Version actualCurrentVersion = versionService.getCurrentVersion();

        assertEquals(null, actualCurrentVersion);
    }

}
