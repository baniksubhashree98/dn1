package com.thoughtworks.DailyNeeds.controller;

import com.thoughtworks.DailyNeeds.entity.Version;
import com.thoughtworks.DailyNeeds.service.VersionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = VersionController.class)
public class VersionControllerTest {

    @MockBean
    private VersionService versionService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldGetTheSameCurrentVersionWhenCurrentVersionIsGiven() throws Exception {
        Version expectedCurrentVersion = Version.builder()
                .versionName("v1")
                .build();
        given(versionService.getCurrentVersion()).willReturn((expectedCurrentVersion));

        ResultActions response = mockMvc.perform(get("/version"));

        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.versionName", is(expectedCurrentVersion.getVersionName())));
    }

    @Test
    public void shouldGetErrorMessageWhenCurrentVersionDoesNotExist() throws Exception {
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get("/version")
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        int status = result.getResponse().getStatus();

        assertEquals("Incorrect Response Status",
                HttpStatus.NOT_FOUND.value(), status);
    }

}

