package com.thoughtworks.DailyNeeds.controller;

import com.thoughtworks.DailyNeeds.DailyNeedsApplication;
import com.thoughtworks.DailyNeeds.entity.Version;
import com.thoughtworks.DailyNeeds.repository.VersionRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = DailyNeedsApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class VersionControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private VersionRepository versionRepository;

    @BeforeEach
    public void before() {
        versionRepository.deleteAll();
    }

    @AfterEach
    public void after() {
        versionRepository.deleteAll();
    }

    @Test
    public void shouldGetCurrentVersionWhenCurrentVersionIsSaved() throws Exception {
        Version expectedCurrentVersion = Version.builder()
                .versionName("v1")
                .build();

        versionRepository.save(expectedCurrentVersion);

        mockMvc.perform(get("/version")).andExpect(status().isOk()).andDo(print()).andExpect(content().json( "{" + "'versionName':" + expectedCurrentVersion.getVersionName() + "}" ));

    }

    @Test
    public void shouldGetErrorMessageWhenCurrentVersionDoesNotExist() throws Exception{
        ResultActions response = mockMvc.perform(get("/version"));

        response.andExpect(status().isNotFound())
                .andDo(print());
    }

}