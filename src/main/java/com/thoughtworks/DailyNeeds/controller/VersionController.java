package com.thoughtworks.DailyNeeds.controller;

import com.thoughtworks.DailyNeeds.entity.Version;
import com.thoughtworks.DailyNeeds.service.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RestController
public class VersionController {

    @Autowired
    private VersionService versionService;

    @Bean
    public WebMvcConfigurer configure() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/*").allowedOrigins("http:localhost:3000");
            }
        };
    }
    @CrossOrigin(origins="http://localhost:3000")
    @GetMapping("/version")
    public ResponseEntity<Version> getCurrentVersion() {
        Version currentVersion = versionService.getCurrentVersion();
        if (currentVersion == null) {
            return new ResponseEntity<Version>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Version>(currentVersion,HttpStatus.OK);
    }

}
