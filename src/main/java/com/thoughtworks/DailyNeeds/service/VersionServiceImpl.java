package com.thoughtworks.DailyNeeds.service;

import com.thoughtworks.DailyNeeds.entity.Version;
import com.thoughtworks.DailyNeeds.repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VersionServiceImpl implements VersionService {

    @Autowired
    private VersionRepository versionRepository;

    @Override
    public Version getCurrentVersion() {
        Version currentVersion = versionRepository.getCurrentVersion();
        return currentVersion;
    }

}
