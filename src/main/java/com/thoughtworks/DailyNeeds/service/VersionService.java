package com.thoughtworks.DailyNeeds.service;

import com.thoughtworks.DailyNeeds.entity.Version;

public interface VersionService {

    Version getCurrentVersion();

}
