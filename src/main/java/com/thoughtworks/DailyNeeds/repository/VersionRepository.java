package com.thoughtworks.DailyNeeds.repository;

import com.thoughtworks.DailyNeeds.entity.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VersionRepository extends JpaRepository<Version, String> {

    @Query(value = "select * from versions v", nativeQuery = true)
    Version getCurrentVersion();

}
