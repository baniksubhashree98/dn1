package com.thoughtworks.DailyNeeds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyNeedsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyNeedsApplication.class, args);
	}

}
